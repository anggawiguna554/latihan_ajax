<?php
    require_once "./config/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Latihan Ajax</title>
    <!-- <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/simple-grid-min.css"> -->
   
  
</head>
<body>
    <h1>Product List</h1>
    <table>
        <thead>
            <tr>
                <th>Produk ID</th>
                <th>Nama Produk</th>
                <th>Deskripsi</th>
                <th>Harga</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody id="load-data">

        </tbody>
    </table>   

    <h1>Insert Data</h1>
    <table>
        <tr>
            <td><input type="hidden" name="getId"></td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td><input type="text" name="inputNama"></td>
        </tr>
        <tr>
            <td>Deskripsi</td>
            <td>:</td>
            <td><input type="text" name="inputDesk"></td>
        </tr>
        <tr>
            <td>Harga</td>
            <td>:</td>
            <td><input type="text" name="inputHarga"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><button onclick="insertData()">Submit</button>
            <button onclick="updateData()">Save Change</button>
            <button onclick="deleteData()">Delete Data</button></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td id="errorMessage"></td>
        </tr>
       
    </table> 


<script src="./js/jquery-3.2.1.js"></script>
<script src="./js/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

    loadData();

    $(document).on("click",".selectData",function () {
    //    console.log("data Diklik"); 
        var id = $(this).attr("id");
        $.ajax({
            type : "POST",
            data : "id="+id,
            url : "doGetData.php",
            success: function(result){
                var resultObject = JSON.parse(result);

                // console.log(resultObject);

                $("[name='getId']").val(resultObject.id);
                $("[name='inputNama']").val(resultObject.nama);
                $("[name='inputDesk']").val(resultObject.deskripsi);
                $("[name='inputHarga']").val(resultObject.harga);
            }
        });
    });

    function deleteData() {
        var inputId = $("[name='getId']").val();
        $.ajax({
            type : "POST",
            data : "id="+inputId,
            url : "deleteData.php",
            success : function(result){
                loadData();
                $("input[name='getId']").val("");
                $("input[name='inputNama']").val("");
                $("input[name='inputDesk']").val("");
                $("input[name='inputHarga']").val("");
            }
        });
    }

    function updateData(){
        var inputId = $("[name='getId']").val();
        var inputNama = $("[name='inputNama']").val();
        var inputDesk = $("[name='inputDesk']").val();
        var inputHarga = $("[name='inputHarga']").val();

        $.ajax({
            type : "POST",
            data : "id="+inputId+"&name="+inputNama+"&deskripsi="+inputDesk+"&harga="+inputHarga,
            url : "updateData.php",
            success: function(result){

                var resultObject = JSON.parse(result);
                $("#errorMessage").html(resultObject.message);

                loadData();
                $("input[name='getId']").val("");
                $("input[name='inputNama']").val("");
                $("input[name='inputDesk']").val("");
                $("input[name='inputHarga']").val("");

            }
        });
    }

    function insertData(){
        var inputNama = $("[name='inputNama']").val();
        var inputDesk = $("[name='inputDesk']").val();
        var inputHarga = $("[name='inputHarga']").val();
        // console.log(inputNama+" "+inputDesk+" "+inputHarga);

        $.ajax({
            type : "POST",
            data : "name="+inputNama+"&deskripsi="+inputDesk+"&harga="+inputHarga,
            url : "process.php",
            success : function (result) {
                // console.log(result);
                var resultObject = JSON.parse(result);
                $("#errorMessage").html(resultObject.message);

                loadData();

                $("input[name='inputNama']").val("");
                $("input[name='inputDesk']").val("");
                $("input[name='inputHarga']").val("");
            }
        });
    }
    function loadData() {
        var dataHandler = $("#load-data");
        dataHandler.html("");
        $.ajax({
            type : "GET",
            data : "",
            url : "getData.php",
            success : function(result){
                var resultObject = JSON.parse(result);
                // console.log(resultObject);
                $.each(resultObject,function(key,val){
                    // console.log(val);

                    var newRow = $("<tr>");
                    newRow.html("<td>"+val.id+"</td><td>"+val.nama+"</td><td>"+val.deskripsi+"</td><td>"+val.harga+"</td><td><button class='selectData' id='"+val.id+"'>Edit</button></td>");

                    dataHandler.append(newRow);
                });
            }
        });
    }
    
</script>
</body>